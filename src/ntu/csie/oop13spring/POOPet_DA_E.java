package ntu.csie.oop13spring;
import java.util.Random;
import java.io.*;
import javax.swing.*;
import java.awt.*;
public class POOPet_DA_E extends POOPet{
	public POOPet_DA_E(){
		setHP(800);
		setMP(1000);
		setAGI(1000);
		setName("大義");
	}
	public POONewCoordinate pos = new POONewCoordinate(0,0);
	int turn=1;
	public POOAction act(POOArena arena){
		System.out.println("position: ("+pos.x+","+pos.y+")");
		for(POOPet element : arena.getAllPets()){
			if(element!=this){
				if(arena.getPosition(element).equals(this.pos)){
					System.out.println("對戰中!!!");
					if(getMP()<100 || getHP()<=300){
						pos = (POONewCoordinate)move(arena);
						System.out.println("逃跑! move to ("+pos.x+","+pos.y+")");
						POOArena_EDARhinos.action.setText("[大義] <對戰中> 逃跑! move to ("+pos.x+","+pos.y+")");
						break;
					}
					else{
						new DaEKick().act(element);
						if(getMP()-400<=0){setMP(getMP()-400);}
						else {setMP(getMP()-400);}
						System.out.println("大義使用犀牛角攻擊");
						POOArena_EDARhinos.action.setText("[大義] <對戰中> 使用 犀牛角攻擊");
						break;
					}
				}
				else{
					System.out.println("休息移動中!!!");
					if(getAGI()<100  ){
						stay();
						System.out.println("stay at ("+pos.x+","+pos.y+")");
						POOArena_EDARhinos.action.setText("[大義] <休息中> "+"stay at ("+pos.x+","+pos.y+")");
						break;
					}
					else{
						pos = (POONewCoordinate)move(arena);
						System.out.println("move to ("+pos.x+","+pos.y+")");
						POOArena_EDARhinos.action.setText("[大義] <移動中> "+"move to ("+pos.x+","+pos.y+")");
						break;
					}				
				}
			}
		}
		return null;
	}
	
	private void stay(){
		setAGI(getAGI()+100);
		setHP(getHP()+10);
		setMP(getMP()+10);
	}
    protected POOCoordinate move(POOArena arena){
		Random ran = new Random();
		int x2,y2;
		do{
			x2=pos.x;
			y2=pos.y;	
			int next = ran.nextInt(4);
			if(next==0){x2++;}
			else if(next==1){y2++;}
			else if(next==2){x2--;}
			else if(next==3){y2--;}
		}while(x2>2 || y2>2 || x2<0 || y2<0);
		if(getAGI()-50<=0){setAGI(0);}
		else {setAGI(getAGI()-50);}
		return new POONewCoordinate(x2,y2);
	}
}
class DaEKick extends POOSkill{
    public void act(POOPet pet){
        int hp = pet.getHP();
        if (hp > 0){
			if(hp-350<=0){pet.setHP(0);}
			else {pet.setHP(hp - 350);}
		}
    }

}