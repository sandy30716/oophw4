package ntu.csie.oop13spring;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.Timer;
import java.util.*;

public class POOArena_EDARhinos extends POOArena{
	public static JFrame frm;
	int turn = 1;
	JPanel jp1 = new JPanel();
	JPanel jp2 = new JPanel();
	JLabel[][] label3 = new JLabel[3][3];
	public static JLabel action = new JLabel();
	public static JLabel action2 = new JLabel();
	public POOArena_EDARhinos(){
		frm = new JFrame();
		frm.setSize(550,680);
		frm.setTitle("Show the Game!!!");
		frm.setResizable(false);
		frm.setLayout(new FlowLayout());
		
		ImageIcon imageIcon = new ImageIcon("DA_E.jpg");
		JLabel label1 = new JLabel();
		label1.setIcon(imageIcon);
		jp1.add(label1);
		
		ImageIcon imageIcon2 = new ImageIcon("Dunky.jpg");
		JLabel label2 = new JLabel();
		label2.setIcon(imageIcon2);
		jp2.add(label2);

		frm.add(jp1);
		frm.add(jp2);
		
		ImageIcon map = new ImageIcon("map.jpg");
		
		
		for(int i = 0;i<3;i++){
			label3[i] = new JLabel[3];
			for(int j=0;j<3;j++){
				label3[i][j] = new JLabel();
				label3[i][j].setIcon(map);
				frm.add(label3[i][j]);
			}
		}
		frm.add(action);
		frm.add(action2);
		label4.setText("<html><br>name:"+"<br>HP:"+"<br>MP:"+"</html>");
		label5.setText("<html><br>name:"+"<br>HP:"+"<br>MP:"+"</html>");
		jp1.add(label4);
		jp1.add(label5);		
		frm.setVisible(true);
	}

	public boolean fight(){
		for(POOPet element : getAllPets()){
			System.out.println("===\n"+element.getName()+" 's turn");
			System.out.println("HP:"+element.getHP()+" MP:"+element.getMP()+" AGI:"+element.getAGI());
			if(element.getHP()<=0){
				if(element instanceof POOPet_DA_E){
					label4.setText("<html><br>name:"+element.getName()+"<br>HP:"+element.getHP()+"<br>MP:"+element.getMP()+"</html>");				
				}
				else if(element instanceof POOPet_Dunky){
					label5.setText("<html><br>name:"+element.getName()+"<br>HP:"+element.getHP()+"<br>MP:"+element.getMP()+"</html>");				
				}
				System.out.println(element.getName()+" is dead.");
				action.setText("*** Game Over ***");
				action2.setText(element.getName()+" is dead.");
				return false;
			}
			else{
				element.act(this);
			}
		}
		return true;
	}
	JLabel label4 = new JLabel();
	JLabel label5 = new JLabel();
	public void show(){
		POOPet[] Pets = getAllPets();		

			label4.setText("<html><br>name:"+Pets[0].getName()+"<br>HP:"+Pets[0].getHP()+"<br>MP:"+Pets[0].getMP()+"</html>");
			label5.setText("<html><br>name:"+Pets[1].getName()+"<br>HP:"+Pets[1].getHP()+"<br>MP:"+Pets[1].getMP()+"</html>");
			if(turn == 1){
				action.setText("*** EDARhinos Arena ***");
				action2.setText(" ");
				for(int i=0;i<5;i++){
					try{Thread.sleep(1000);}
					catch(InterruptedException e){}
					action2.setText(new String(""+(5-i)));	
				}	
					try{Thread.sleep(1000);}
					catch(InterruptedException e){}
					action2.setText("Game Start!");	
					try{Thread.sleep(1000);}
					catch(InterruptedException e){}					
				turn = 0;
			}

		
		ImageIcon map = new ImageIcon("map.jpg");
		ImageIcon DA_E = new ImageIcon("DA_E_only.jpg");	
		ImageIcon Dunky = new ImageIcon("Dunky_only.jpg");			
		ImageIcon Both = new ImageIcon("both.jpg");	
		for(int i = 0;i<3;i++){
			for(int j=0;j<3;j++){
				if(getPosition(Pets[0]).equals(getPosition(Pets[1])) &&  getPosition(Pets[0]).x == i && getPosition(Pets[0]).y == j){
					label3[i][j].setIcon(Both);
				}
				else{
					if(getPosition(Pets[0]).x == i && getPosition(Pets[0]).y == j){label3[i][j].setIcon(DA_E);}
					else if(getPosition(Pets[1]).x == i && getPosition(Pets[1]).y == j){label3[i][j].setIcon(Dunky);}
					else{label3[i][j].setIcon(map);}
				}
			}
		}
        try {Thread.sleep(1000);}
        catch(InterruptedException e){}
			 
	}
    public POOCoordinate getPosition(POOPet p){
		if(p instanceof POOPet_DA_E){
			POOPet_DA_E newp = (POOPet_DA_E)p;
			POOCoordinate position = newp.pos;
			return position;
		}
		else if(p instanceof POOPet_Dunky){
			POOPet_Dunky newp = (POOPet_Dunky)p;
			POOCoordinate position = newp.pos;
			return position;
		}
		else {return new POONewCoordinate(3,3);}
	}
	
}

